package chhatrasal.android.app.techwrightlabstest.model

import java.lang.Exception

data class OrderDetailsResponse(
    val success: OrderDetails? = null,
    val error: String? = null
)