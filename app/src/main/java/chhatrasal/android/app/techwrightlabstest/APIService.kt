package chhatrasal.android.app.techwrightlabstest

import chhatrasal.android.app.techwrightlabstest.model.OrderDetails
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface APIService {

    @GET("orders/{order_id}")
    fun getOrderDetails(@Path("order_id") orderId: String): Observable<OrderDetails>

    @GET("orders/{order_id}/verify")
    fun verifyOrderDetails(@Path("order_id") orderId: String): Observable<OrderDetails>
}