package chhatrasal.android.app.techwrightlabstest.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import chhatrasal.android.app.techwrightlabstest.R
import chhatrasal.android.app.techwrightlabstest.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.loader

class LoginActivity : AppCompatActivity() {

    private var mLoginViewModel: LoginViewModel? = null
    private var mSharedPreference: SharedPreferences? = null
    @SuppressLint("ApplySharedPref")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mSharedPreference = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val isUserLoggedIn = mSharedPreference?.getBoolean(KEY_IS_USER_LOGGED_IN, false)
        if (isUserLoggedIn == true) {
            startActivity(Intent(this@LoginActivity, QRScannerActivity::class.java))
            finish()
        } else {
            setContentView(R.layout.activity_main)
            val username = findViewById<EditText>(R.id.user_name_edit_text)
            val password = findViewById<EditText>(R.id.password_edit_text)
            mLoginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)

            login_button?.isEnabled = false
            mLoginViewModel?.loginResponse?.observe(this, Observer {
                val loginResult = it ?: return@Observer
                hideLoader()
                login_button?.isEnabled = true
                if (loginResult.error != null) {
                    Toast.makeText(this@LoginActivity, "${loginResult.error}", Toast.LENGTH_LONG).show()
                    return@Observer
                }
                if (loginResult.success != null) {
                    mSharedPreference?.edit()?.putBoolean(KEY_IS_USER_LOGGED_IN, true)?.commit()
                    startActivity(Intent(this@LoginActivity, QRScannerActivity::class.java))
                    finish()
                }
            })

            login_button?.setOnClickListener {
                showLoader()
                mLoginViewModel?.loginUser(username.text.toString(), password.text.toString())
            }

            username.afterTextChanged {
                login_button?.isEnabled =
                    !TextUtils.isEmpty(it) && !TextUtils.isEmpty(password.text) && username?.text?.length ?: 0 > 5 && password?.text?.length?:0 > 5
                if (it.length in 1..5) {
                    username_layout?.error = "Username should be > 5 character"
                    username_layout?.isErrorEnabled = true
                } else {
                    username_layout?.isErrorEnabled = false
                }
            }

            password.apply {
                afterTextChanged {
                    login_button?.isEnabled = !TextUtils.isEmpty(it) && !TextUtils.isEmpty(password.text) && username?.text?.length ?: 0 > 5 && password?.text?.length?:0 > 5
                    if (it.length in 1..5) {
                        password_layout?.isErrorEnabled = true
                        password_layout?.error = "Password should be > 5 character"
                    } else {
                        password_layout?.isErrorEnabled = false
                    }
                }
                setOnEditorActionListener { _, actionId, _ ->
                    when (actionId) {
                        EditorInfo.IME_ACTION_DONE -> {
                            showLoader()
                            mLoginViewModel?.loginUser(
                                username.text.toString(),
                                password.text.toString()
                            )
                        }
                    }
                    false
                }
            }
        }
    }

    private fun showLoader() {
        loader?.visibility = View.VISIBLE
    }

    private fun hideLoader() {
        loader?.visibility = View.GONE
    }

    companion object {
        private const val KEY_IS_USER_LOGGED_IN = "isUserLoggedIn"
        private const val SHARED_PREF_NAME = "login"
    }
}

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            afterTextChanged.invoke(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }
    })
}
