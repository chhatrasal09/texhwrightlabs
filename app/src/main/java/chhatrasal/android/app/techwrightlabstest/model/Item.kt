package chhatrasal.android.app.techwrightlabstest.model

data class Item(
    val brand_name: String,
    val collection: String?,
    val colour_name: String?,
    val display_image: String?,
    val item_fit: String?,
    val material: String?,
    val mrp: Int = 0,
    val product_name: String,
    val size: String?,
    val tax_percent: Int = 0
)