package chhatrasal.android.app.techwrightlabstest.utils

import android.content.Context
import android.util.TypedValue
import kotlin.math.roundToInt

object Utility {

    fun dpToPx(context: Context, dp: Int): Int {
        val px = dpToPx(context, java.lang.Float.valueOf(dp.toString()))
        return px.roundToInt()
    }

    fun dpToPx(context: Context, dipValue: Float): Float {
        val metrics = context.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics)
    }

    fun screenWidth(context: Context): Int = context.resources.displayMetrics.widthPixels
    fun screenHeight(context: Context): Int = context.resources.displayMetrics.heightPixels
}