package chhatrasal.android.app.techwrightlabstest.model

data class Order(
    val created_at: String,
    val currency: String,
    val gross_amount: Int,
    val items: List<ItemList>,
    val net_amount: Int,
    val order_no: String,
    val quantity: Int,
    val razorpay_order_id: String?,
    val status: String,
    val store: String,
    val total_tax: Int,
    val tracking_id: String
)