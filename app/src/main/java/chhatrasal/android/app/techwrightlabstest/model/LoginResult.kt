package chhatrasal.android.app.techwrightlabstest.model

data class LoginResult(
    val success: User? = null,
    val error: String? = null
)