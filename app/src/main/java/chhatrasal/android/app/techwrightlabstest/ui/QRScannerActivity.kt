package chhatrasal.android.app.techwrightlabstest.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.SurfaceHolder
import android.widget.Toast
import androidx.core.app.ActivityCompat
import chhatrasal.android.app.techwrightlabstest.R
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import kotlinx.android.synthetic.main.activity_qrscanner.*
import com.google.android.gms.vision.CameraSource
import androidx.core.content.ContextCompat
import androidx.core.util.valueIterator
import chhatrasal.android.app.techwrightlabstest.utils.Utility
import com.google.android.gms.vision.Detector


class QRScannerActivity : AppCompatActivity() {

    private var mBarcodeDetector: BarcodeDetector? = null
    private var mCameraSource: CameraSource? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qrscanner)

        mBarcodeDetector = BarcodeDetector.Builder(this).setBarcodeFormats(Barcode.QR_CODE).build()
        if (mBarcodeDetector?.isOperational == false) {
            Toast.makeText(this, "Coiuld not set up barcode scanner", Toast.LENGTH_LONG).show()
        }

        mCameraSource = CameraSource.Builder(this, mBarcodeDetector)
            .setRequestedPreviewSize(Utility.screenWidth(this), Utility.screenHeight(this))
            .setAutoFocusEnabled(true)
            .setFacing(CameraSource.CAMERA_FACING_BACK)
            .build()

        barcode_camera_view?.holder?.addCallback(mCameraSurfaceHolderCallback)
        mBarcodeDetector?.setProcessor(mOnBarcodeDetected)
    }

    override fun onStart() {
        super.onStart()
        barcode_camera_view?.holder?.addCallback(mCameraSurfaceHolderCallback)
        mBarcodeDetector?.setProcessor(mOnBarcodeDetected)
    }

    override fun onStop() {
        super.onStop()
        barcode_camera_view?.holder?.removeCallback(mCameraSurfaceHolderCallback)
        mBarcodeDetector?.setProcessor(null)
        mBarcodeDetector?.release()
    }

    private val mCameraSurfaceHolderCallback = object : SurfaceHolder.Callback {
        override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
        }

        override fun surfaceDestroyed(holder: SurfaceHolder?) {
            mCameraSource?.stop()
        }

        override fun surfaceCreated(holder: SurfaceHolder?) {
            try {
                if (ContextCompat.checkSelfPermission(
                        this@QRScannerActivity,
                        Manifest.permission.CAMERA
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    mCameraSource?.start(barcode_camera_view?.holder)
                } else {
                    ActivityCompat.requestPermissions(
                        this@QRScannerActivity,
                        arrayOf(Manifest.permission.CAMERA),
                        CAMERA_PERMISSION_CONSTANT
                    )
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.v("#####surface Created", "@ " + e.message)
            }

        }

    }

    private val mOnBarcodeDetected = object : Detector.Processor<Barcode> {
        override fun release() {

        }

        override fun receiveDetections(detections: Detector.Detections<Barcode>?) {
            val barcodeList = detections?.detectedItems
            if (barcodeList != null && barcodeList.size() > 0) {
                for (barcode in barcodeList.valueIterator()) {
                    mBarcodeDetector?.setProcessor(null)
                    mBarcodeDetector?.release()
                    this@QRScannerActivity.runOnUiThread {
                        val activityIntent = Intent(this@QRScannerActivity, OrderDetailsActivity::class.java)
                        activityIntent.putExtra("order_id", barcode.displayValue)
                        startActivity(activityIntent)
                    }
                }
            }
        }

    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_PERMISSION_CONSTANT) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mCameraSource?.start(barcode_camera_view?.holder)
            } else {
                finish()
            }
        } else {
            finish()
        }
    }

    companion object {
        private const val CAMERA_PERMISSION_CONSTANT = 100
    }
}
