package chhatrasal.android.app.techwrightlabstest.repository

import androidx.lifecycle.MutableLiveData
import chhatrasal.android.app.techwrightlabstest.model.LoginResult
import chhatrasal.android.app.techwrightlabstest.model.Result
import chhatrasal.android.app.techwrightlabstest.model.User
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import kotlin.math.abs

class LoginRepository private constructor() {

    var mUser: MutableLiveData<LoginResult> = MutableLiveData()
    val isLoggedIn: Boolean
        get() = mUser.value != null


    fun login(username: String, password: String) {
        Observable.just(username)
            .subscribeOn(Schedulers.io())
            .delay(5000, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableObserver<Any>() {
                override fun onComplete() {

                }

                override fun onNext(t: Any) {
                    if (username.equals(password, ignoreCase = false) && username.length > 5) {
                        mUser.value = LoginResult(success = User("abs", "abs"))
                    } else {
                        mUser.value = LoginResult(error = "Please enter valid credentials")
                    }
                }

                override fun onError(e: Throwable) {
                    mUser.value = LoginResult(error = e.message)
                }
            })
    }

    fun logOutUser() {
        this.mUser.value = null
    }

    companion object {
        private var instance: LoginRepository? = null
        fun getInstance(): LoginRepository {
            if (instance == null) {
                instance = LoginRepository()
            }
            return instance!!
        }

    }
}