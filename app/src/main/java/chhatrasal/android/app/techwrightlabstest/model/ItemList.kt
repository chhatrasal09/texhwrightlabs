package chhatrasal.android.app.techwrightlabstest.model

data class ItemList(
    val item: Item,
    val quantity: Int
)