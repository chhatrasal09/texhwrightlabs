package chhatrasal.android.app.techwrightlabstest.model

data class User(val userId: String, val displayName: String)