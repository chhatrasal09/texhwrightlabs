package chhatrasal.android.app.techwrightlabstest.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import chhatrasal.android.app.techwrightlabstest.R
import chhatrasal.android.app.techwrightlabstest.model.ItemList
import chhatrasal.android.app.techwrightlabstest.utils.Utility
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.order_item_view.view.*

class OrderAdapter(private val mContext: Context, private val mList: MutableList<ItemList>) :
    RecyclerView.Adapter<OrderAdapter.OrderViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder {
        return OrderViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.order_item_view, parent, false))
    }

    override fun getItemCount(): Int = mList.size

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        val item = mList[position].item
        holder.mRootView.product_name?.text = item.product_name
        Glide.with(mContext)
            .load(item.display_image)
            .apply(RequestOptions().override(Utility.dpToPx(mContext, 10), Utility.dpToPx(mContext, 100)).centerCrop())
            .apply(RequestOptions.bitmapTransform(RoundedCorners(Utility.dpToPx(mContext, 12))))
            .into(holder.mRootView.product_image)
        holder.mRootView.brand_name?.text = item.brand_name
        holder.mRootView.quantity?.text = "${mList[position].quantity}"
        holder.mRootView.price?.text = "${item.mrp}"
        holder.mRootView.item_total?.text = "${item.mrp.times(mList[position].quantity)}"
        holder.mRootView.tax_percent?.text =
            "${(item.mrp.times(mList[position].quantity)).times(mList[position].item.tax_percent)}"
        if (item.size != null) {
            holder.mRootView.size_text?.visibility = View.VISIBLE
            holder.mRootView.size?.visibility = View.VISIBLE
            holder.mRootView.size?.text = "${item.size}"
        } else {
            holder.mRootView.size_text?.visibility = View.GONE
            holder.mRootView.size?.visibility = View.GONE
        }
        if (item.colour_name != null) {
            holder.mRootView.color_text?.visibility = View.VISIBLE
            holder.mRootView.color?.visibility = View.VISIBLE
            holder.mRootView.color?.text = "${item.colour_name}"
        } else {
            holder.mRootView.color_text?.visibility = View.GONE
            holder.mRootView.color?.visibility = View.GONE
        }
    }

    class OrderViewHolder(val mRootView: View) : RecyclerView.ViewHolder(mRootView) {
    }
}
