package chhatrasal.android.app.techwrightlabstest.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import chhatrasal.android.app.techwrightlabstest.model.LoginResult
import chhatrasal.android.app.techwrightlabstest.model.Result
import chhatrasal.android.app.techwrightlabstest.repository.LoginRepository

class LoginViewModel : ViewModel() {

    private val mLoginRepo: LoginRepository = LoginRepository.getInstance()
    val loginResponse: MutableLiveData<LoginResult> = mLoginRepo.mUser

    fun loginUser(userName: String, password: String) {
        mLoginRepo.login(userName, password)
    }
}