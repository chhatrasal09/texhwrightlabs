package chhatrasal.android.app.techwrightlabstest.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import chhatrasal.android.app.techwrightlabstest.R
import chhatrasal.android.app.techwrightlabstest.adapter.OrderAdapter
import chhatrasal.android.app.techwrightlabstest.model.ItemList
import chhatrasal.android.app.techwrightlabstest.model.OrderDetails
import chhatrasal.android.app.techwrightlabstest.viewmodel.OrderViewModel
import kotlinx.android.synthetic.main.activity_order_details.*
import java.util.*

class OrderDetailsActivity : AppCompatActivity() {

    private var mOrderViewModel: OrderViewModel? = null
    private val mItemList: MutableList<ItemList> = mutableListOf()
    private var mOrderId: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_details)

        mOrderViewModel = ViewModelProviders.of(this).get(OrderViewModel::class.java)

        if (intent.hasExtra(KEY_ORDER_ID)) {
            mOrderId = intent.getStringExtra(KEY_ORDER_ID)
            showProgressBar()
            mOrderViewModel?.order?.observe(this, Observer {
                val result = it ?: return@Observer
                hideProgressBar()
                if (result.error != null) {
                    // show error view
                }
                if (result.success != null) {
                    populateOrderDetails(result.success)
                }
            })

            verify_button?.setOnClickListener {
                showProgressBar()
                it.isEnabled = false
                mOrderViewModel?.verifyOrderDetail(mOrderId)
            }
            mOrderViewModel?.getOrderDetail(mOrderId)
        }

        val adapter = OrderAdapter(this, mItemList)
        item_recycler_view?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        item_recycler_view?.adapter = adapter
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        item_recycler_view?.addItemDecoration(itemDecoration)

    }

    private fun showProgressBar() {
        loader?.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        loader?.visibility = View.GONE
    }

    private fun getOrderStatus(input: String): String {
        return when {
            "order_completed".equals(input, ignoreCase = false) -> {
                verify_button?.text = "Verified"
                Toast.makeText(this@OrderDetailsActivity, "Order verified!", Toast.LENGTH_LONG).show()
                verify_button?.isClickable = false
                verify_button?.isEnabled = false
                verify_button?.background =
                    ContextCompat.getDrawable(this@OrderDetailsActivity, R.drawable.rounded_corner_verify)
                "Order Completed"
            }
            "checked_out".equals(input, ignoreCase = false) -> {
                verify_button?.text = "Verify"
                verify_button?.isClickable = true
                verify_button?.isEnabled = true
                verify_button?.background =
                    ContextCompat.getDrawable(this@OrderDetailsActivity, R.drawable.rounded_corner_track_now)
                "Checked out"
            }
            "order_failed".equals(input, ignoreCase = false) -> {
                verify_button?.text = "Verify"
                verify_button?.isClickable = true
                verify_button?.isEnabled = true
                verify_button?.background =
                    ContextCompat.getDrawable(this@OrderDetailsActivity, R.drawable.rounded_corner_track_now)
                "Order Failed"
            }
            else -> ""
        }
    }

    private fun populateOrderDetails(orderDetails: OrderDetails) {
        mItemList.clear()
        mItemList.addAll(orderDetails.order.items)
        total_amount?.text = "${orderDetails.order.net_amount}"
        tax?.text = "${orderDetails.order.total_tax}"
        amount_paid?.text = "${orderDetails.order.gross_amount}"
        item_recycler_view?.adapter?.notifyDataSetChanged()
        store_name?.text = orderDetails.order.store
        order_date?.text = orderDetails.order.created_at
        order_number?.text = orderDetails.order.tracking_id
        amount_text?.text = "Amount paid (in ${orderDetails.order.currency})"
        tax_text?.text = "Tax payable (in ${orderDetails.order.currency})"
        total_amount_text?.text = "Total Amount (in ${orderDetails.order.currency})"
        transaction_id?.text = orderDetails.order.razorpay_order_id
        order_status?.text = getOrderStatus(orderDetails.order.status)
    }

    companion object {
        private const val KEY_ORDER_ID = "order_id"
    }
}

