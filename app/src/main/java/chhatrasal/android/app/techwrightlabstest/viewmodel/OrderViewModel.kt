package chhatrasal.android.app.techwrightlabstest.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import chhatrasal.android.app.techwrightlabstest.repository.OrderRepository

class OrderViewModel : ViewModel() {

    private var mServerRepo: OrderRepository = OrderRepository.getInstance()
    val order = mServerRepo.orderDetail

    fun getOrderDetail(orderId: String) {
        mServerRepo.getOrderDetails(orderId)
    }

    fun verifyOrderDetail(orderId: String) {
        mServerRepo.verifyOrderDetails(orderId)
    }
}