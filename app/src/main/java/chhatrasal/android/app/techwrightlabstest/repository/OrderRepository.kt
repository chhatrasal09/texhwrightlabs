package chhatrasal.android.app.techwrightlabstest.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import chhatrasal.android.app.techwrightlabstest.APIService
import chhatrasal.android.app.techwrightlabstest.model.OrderDetails
import chhatrasal.android.app.techwrightlabstest.model.OrderDetailsResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class OrderRepository {

    var orderDetail: MutableLiveData<OrderDetailsResponse> = MutableLiveData()
    private val mApiService: APIService = Retrofit
        .Builder()
        .baseUrl("http://poneglyph.techwright.io/api/v1/")
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(APIService::class.java)

    fun getOrderDetails(orderId: String) {
        mApiService.getOrderDetails(orderId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableObserver<OrderDetails>() {
                override fun onComplete() {

                }

                override fun onNext(orderDetails: OrderDetails) {
                    orderDetail.value = OrderDetailsResponse(success = orderDetails)
                }

                override fun onError(e: Throwable) {
                    Log.e("Error getOrderDetails", "${e.message}")
                    orderDetail.value = OrderDetailsResponse(error = e.message)
                }
            })
    }

    fun verifyOrderDetails(orderId: String) {
        mApiService.verifyOrderDetails(orderId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableObserver<OrderDetails>() {
                override fun onComplete() {

                }

                override fun onNext(orderDetails: OrderDetails) {
                    orderDetail.value = OrderDetailsResponse(success = orderDetails)
                }

                override fun onError(e: Throwable) {
                    Log.e("Error getOrderDetails", "${e.message}")
                    orderDetail.value = OrderDetailsResponse(error = e.message)
                }
            })
    }

    companion object {
        private var instance: OrderRepository? = null
        fun getInstance(): OrderRepository {
            if (instance == null) {
                instance = OrderRepository()
            }
            return instance!!
        }

    }
}